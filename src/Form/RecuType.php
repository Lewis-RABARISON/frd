<?php

namespace App\Form;

use App\Entity\Part;
use App\Entity\Recu;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class RecuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('date')
            ->add('monRecu', TextType::class,[
                'label'=> 'Crédit'
            ])
            ->add('obs',TextareaType::class,[
                'label'=> 'Observation',
                'required'=> false
            ])
            ->add('client', EntityType::class,[
                'class' => Part::class,
                'choice_label' => 'Nom'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Recu::class,
        ]);
    }
}
