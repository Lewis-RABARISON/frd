<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('NumCom',TextType::class,[
                'label' => 'Numéro de compte'
            ])
            ->add('Nom',TextType::class)
            ->add('Prenom',TextType::class,[
                'label'=>'Prénom'
            ])
            ->add('Tel',TextType::class,[
                'label'=>'Téléphone'
            ])
            ->add('email', EmailType::class)
            ->add('Password',PasswordType::class, [
                'label' => 'Mot de passe'
            ])
            ->add('Confir_pwd',PasswordType::class, [
                'label' => 'Confirmer le mot de passe'
            ])
            ->add('picture',FileType::class,[
                'label' => 'Mettez un photo...',
                'required' => false,
                'mapped' => false,
                'attr' => [
                    'class' => 'image-preview'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
