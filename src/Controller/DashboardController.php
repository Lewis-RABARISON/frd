<?php

namespace App\Controller;

use App\Repository\PartRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DashboardController extends AbstractController
{
    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function index(UserRepository $userRepository,
                        PartRepository $partRepository): Response
    {

        $users = count($userRepository->findAll());
        $part = count($partRepository->findAll());

        return $this->render('dashboard/index.html.twig',
                             compact('users','part')
            );
    }
}
