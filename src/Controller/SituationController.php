<?php

namespace App\Controller;

use App\Repository\RecuRepository;
use App\Repository\UserRepository;
use App\Repository\DepenseRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SituationController extends AbstractController
{
    /**
     * @Route("/situation", name="situation")
     */
    public function index(UserRepository $userRepository,
                        RecuRepository $recuRepository,
                        DepenseRepository $depenseRepository): Response
    {
        return $this->render('situation/index.html.twig', [
            'users' => $userRepository->findAll(),
            'recus' => $recuRepository->findAll(),
            'depenses'=>$depenseRepository->findAll()
        ]);
    }
}
