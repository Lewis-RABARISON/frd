<?php

namespace App\Controller;

use App\Entity\Recu;
use App\Form\RecuType;
use App\Repository\RecuRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RecuController extends AbstractController
{
    /**
     * @Route("/recu", name="recu")
     */
    public function index(RecuRepository $recuRepository): Response
    {
        return $this->render('recu/index.html.twig', [
            'recus' => $recuRepository->findAll(),
        ]);
    }

    /**
     * @Route("/solde-recu", name="solde-recu")
     */
    public function solde(Request $request,EntityManagerInterface $manager)
    {
        $recu = new Recu();
        $form = $this->createForm(RecuType::class, $recu);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            
            $manager->persist($recu);
            $manager->flush();

            return $this->redirectToRoute('recu');
        }

        return $this->render("recu/solde.html.twig",[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/mise-à-jour", name="maj")
     */
    public function maj(Request $request,EntityManagerInterface $manager, Recu $recu)
    {
        $form = $this->createForm(RecuType::class, $recu);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            
            $manager->persist($recu);
            $manager->flush();

            return $this->redirectToRoute('recu');
        }

        return $this->render("recu/solde.html.twig",[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/suppression", name="suppression")
     */
    public function delete(EntityManagerInterface $manager, Recu $recu)
    {
        $manager->remove($recu);
        $manager->flush();

        return $this->redirectToRoute('recu');
    }
}
