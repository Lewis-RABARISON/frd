<?php

namespace App\Controller\MemberArea;

use App\Entity\User;
use App\Form\RegistrationType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/utilisateur", name="user")
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/ajout-utilisateur", name="inscription")
     */
    public function incription(Request $request, EntityManagerInterface $manager,UserPasswordEncoderInterface $encoder)
    {

        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            $manager->persist($user);
            $manager-> flush();
            $this->addFlash(
                'success',
                "L’<strong>utilisateur</strong> a été bien enregistrer"
            );
            return $this->redirectToRoute('user');
        }
        return $this->render('security/inscr.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/profile", name="profile")
     */
    public function profil(User $user)
    {
        return $this->render('security/profil.html.twig',[
            'users'=>$userRepository->findBy()
        ]);
    }

    /**
     * @Route("/{id}/edite-le-role d'utilisateur", name="edite_user")
     */
    public function editer(Request $request, EntityManagerInterface $manager,UserPasswordEncoderInterface $encoder,User $user)
    {
        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            $manager->persist($user);
            $manager-> flush();

            return $this->redirectToRoute('user');
        }
        return $this->render('user/edit.html.twig',[
            'form' => $form->createView()
        ]);
}

    // /**
    //  * @Route("/{id}/user-delete", name="user_delete")
    //  */
    // public function delete(EntityManagerInterface $manager, User $user)
    // {
    //     $manager->remove($user);
    //     $manager->flush();

    //     return $this->redirectToRoute("user");
    // }
}
