<?php

namespace App\Controller;

use App\Entity\Part;
use App\Form\PartType;
use App\Repository\PartRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PartController extends AbstractController
{
    /**
     * @Route("/partenaire", name="part")
     */
    public function index(PartRepository $partRepository): Response
    {
        return $this->render('part/index.html.twig', [
            'parts' => $partRepository->findAll(),
        ]);
    }

    /**
     * @Route("/ajout-part", name="part_new")
     */
    public function create(Request $request, EntityManagerInterface $manager)
    {   
        $part = new Part();
        $form = $this->createForm(PartType::class, $part);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($part);
            $manager->flush();

            return $this->redirectToRoute("part");
        }
                
        return $this->render("part/add.html.twig",[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="part_edit")
     */
    public function edit(Request $request, EntityManagerInterface $manager,Part $part){
        $form = $this->createForm(PartType::class, $part);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($part);
            $manager->flush();

            return $this->redirectToRoute("part");
        }

        return $this->render("part/add.html.twig",[
            'form' => $form->createView()
        ]);
    } 

    /**
     * @Route("/{id}/delete", name="part_suppr")
     */
    public function delete(EntityManagerInterface $manager,Part $part)
    {
        $manager->remove($part);
        $manager->flush();

        return $this->redirectToRoute('part');
    }
}
