<?php

namespace App\Controller;

use App\Entity\Depense;
use App\Form\DepenseType;
use App\Repository\DepenseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DepenseController extends AbstractController
{
    /**
     * @Route("/depense", name="depense")
     */
    public function index(DepenseRepository $depenseRepository): Response
    {
        return $this->render('depense/index.html.twig', [
            'depenses' => $depenseRepository->findAll(),
        ]);
    }

    /**
     * @Route("/solde-depense", name="solde_depende")
     */
    public function depense(Request $request,EntityManagerInterface $manager)
    {
        $depense = new depense();
        $form = $this->createForm(DepenseType::class, $depense);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($depense);
            $manager->flush();

            return $this->redirectToRoute('depense');
        }
        return $this->render("depense/solde.html.twig",[
            'form'=> $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/supprimer-solde-depense", name="suppr_solde_depense")
     */
    public function supprimer(EntityManagerInterface $manager, Depense $depense){
        $manager->remove($depense);
        $manager->flush();
        
        return $this->redirectToRoute('depense');
    }
}
