<?php

namespace App\Entity;

use App\Repository\RecuRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RecuRepository::class)
 */
class Recu
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Part::class, inversedBy="recus")
     */
    private $client;

    /**
     * @ORM\Column(type="float")
     */
    private $monRecu;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $obs;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    public function __construct()
    {
        $this->date=new \DateTime();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClient(): ?Part
    {
        return $this->client;
    }

    public function setClient(?Part $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getMonRecu(): ?float
    {
        return $this->monRecu;
    }

    public function setMonRecu(float $monRecu): self
    {
        $this->monRecu = $monRecu;

        return $this;
    }

    public function getObs(): ?string
    {
        return $this->obs;
    }

    public function setObs(?string $obs): self
    {
        $this->obs = $obs;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

}
